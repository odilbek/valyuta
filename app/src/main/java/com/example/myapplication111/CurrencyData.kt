package com.example.myapplication111

data class CurrencyData (
    val success: Boolean,
    val currencies: Map<String, String>
)