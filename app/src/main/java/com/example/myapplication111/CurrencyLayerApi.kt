package com.example.myapplication111

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyLayerApi {
    @GET("list")
    fun getCurrencies(
        @Query("access_key") accessKey: String
    ): Call<CurrencyData>
}