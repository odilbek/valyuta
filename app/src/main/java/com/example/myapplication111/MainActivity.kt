package com.example.myapplication111

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private val currencyLayerApi = RetrofitClient.create()
    private lateinit var currencyListView: ListView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        currencyListView = findViewById(R.id.currencyListView)
        getCurrencies()
    }

    private fun getCurrencies() {
        val call = currencyLayerApi.getCurrencies("2Gmm1JBPW2OPYE8GTS7vmMeiKL4LiKMV")
        call.enqueue(object : Callback<CurrencyData> {
            override fun onResponse(call: Call<CurrencyData>, response: Response<CurrencyData>) {
                if (response.isSuccessful) {
                    val currencyData = response.body()
                    if (currencyData != null) {
                        val currencies = currencyData.currencies
                        val currencyNames = currencies.values.toList()
                        val adapter = ArrayAdapter(
                            this@MainActivity,
                            android.R.layout.simple_list_item_1,
                            currencyNames
                        )
                        currencyListView.adapter = adapter
                    }
                }
            }

            override fun onFailure(call: Call<CurrencyData>, t: Throwable) {
                // Handle the error
            }
        })
    }
}
